# 🚢 Ship Your Enemies Coronavirus 🦠

Welcome to **Ship Your Enemies Coronavirus**, a unique and quirky project designed to spread some fun (and a little bit of mischief) by allowing you to "send" the coronavirus to your friends and enemies! Built with the powerful [Hugo] 🛠️ static site generator and styled with the sleek [Krypton] theme ✨, this project is both fun and functional. Let's get started!

## 🌟 Features

- **Send a Virus**: Choose your target, add a message, and "send" them the coronavirus.
- **Customizable**: Easily modify the site content and appearance.
- **Fast and Lightweight**: Built with Hugo for blazing-fast performance.

## 🔧 Requirements

Before you begin, make sure you have the following tools installed:

- **Hugo** `>= 0.68.3` 📦: The static site generator.
- **Yarn** `>= 1.22.4` 🧶: The package manager for dependencies.

## 🚀 Getting Started

Follow these steps to set up and run the project locally.

### 1. Clone the Repository 🖥️

First, clone the repository to your local machine:

```bash
git clone https://github.com/your-username/shipyourenemiescoronavirus.git
cd shipyourenemiescoronavirus
