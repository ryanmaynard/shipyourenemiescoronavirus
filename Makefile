.PHONY: lint
lint: clean build
	vnu --version
	vnu --Werror --skip-non-html public/

.PHONY: build
build:
	hugo version
	hugo --gc --minify

.PHONY: clean
clean:
	rm -rf public resources
